/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/AbstractDeployer.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/Architecture.hpp"
#include "mauve/runtime/Manager.hpp"
#include "mauve/runtime/Task.hpp"
#include "mauve/runtime/logging/logger.hpp"
#include "mauve/runtime/tracing.hpp"

#include <unistd.h>
#include <string.h>

namespace mauve {
  namespace runtime {

    AbstractDeployer* AbstractDeployer::deployer = nullptr;

    // ------------------------- Constructor -------------------------

    AbstractDeployer::AbstractDeployer()
    : _logger { new DeployerLogger() }
    , start_time { 0 }
    {
      sigemptyset(&sig_set);
      sigaddset(&sig_set, DEPLOYER_SIG);
      if (sigprocmask(SIG_SETMASK, &sig_set, NULL) != 0) {
        _logger->warn("deployer_setsigmask {} {}", errno, strerror(errno));
      }
    }

    // ------------------------- Destructor -------------------------

    AbstractDeployer::~AbstractDeployer() noexcept {
      _logger->info("stopping Deployer");
      stop();
      clear_tasks();
    }

    // ------------------------- Method -------------------------

    time_ns_t AbstractDeployer::now() const {
      struct timespec time;
      if (clock_gettime(MAUVE_CLOCK_ID, &time) == -1) {
        _logger->error("clock_gettime {} {}", errno, strerror(errno));
        return 0;
      }
      return timespec_to_ns(time);
    }

    time_ns_t AbstractDeployer::get_time() const {
      return start_time;
    }

    // ---------- Components & Tasks ----------

    Task* AbstractDeployer::get_task(AbstractComponent * component) {
      return tasks[component];
    }

    bool AbstractDeployer::create_tasks() {
      if (get_architecture() == nullptr) return false;

      clear_tasks();

      for (AbstractComponent * cpt: get_architecture()->get_components()) {
        _logger->debug("add task {}", cpt->name());
        Task* task = new Task(cpt);
        tasks[cpt] = task;
      }

      return true;
    }

    bool AbstractDeployer::activate() {
      if (get_architecture() == nullptr) return false;

      bool ok = true;

      for (auto tuple: tasks) {
        Task* task = tuple.second;
        _logger->debug("activate task {}", task->get_component()->name());
        ok = ok && task->activate();
      }
      return ok;
    }

    bool AbstractDeployer::activate(AbstractComponent * component) {
      if (get_architecture() == nullptr) return false;

      auto search = tasks.find(component);
      if (search != tasks.end()) {
        Task* task = search->second;
        _logger->debug("activate task {}", task->get_component()->name());
        return task->activate();
      }

      return false;
    }

    bool AbstractDeployer::activate(std::string name) {
      for (auto tuple: tasks) {
        AbstractComponent* cpt = tuple.first;
        if (cpt->name() == name) {
          return activate(cpt);
        }
      }
      return false;
    }

    bool AbstractDeployer::start() {
      if (get_architecture() == nullptr) return false;

      // activate tasks
      uint32_t active_tasks = 0;
      for (auto tuple: tasks) {
        Task* task = tuple.second;
        if (task != nullptr) {
          if (task->get_status() == ACTIVATED) {
            active_tasks++;
          }
        }
      }

      // create Barrier
      pthread_barrier_t barrier;
      pthread_barrierattr_t attr;
      pthread_barrierattr_init(&attr);
      int res = pthread_barrier_init(&barrier, &attr, active_tasks+1);
      if (res != 0) {
        _logger->warn("pthread_barrier_init {} {}", errno, strerror(errno));
      }

      // start deployer
      struct timespec time;
      if (clock_gettime(MAUVE_CLOCK_ID, &time) == -1) {
        _logger->warn("clock_gettime {} {}", errno, strerror(errno));
      }
      start_time = timespec_to_ns(time);
      _logger->info("deployer started at {}", start_time);

      // start active tasks
      _logger->debug("starting {} tasks", active_tasks);
      for (auto tuple: tasks) {
        Task* task = tuple.second;
        _logger->debug("start task {}", task->get_component()->name());
        trace_component_start(task->get_component());
        task->start(start_time, &barrier);
      }
      pthread_barrier_wait(&barrier);

      // clear barrier
      pthread_barrier_destroy(&barrier);

      return true;
    }

    bool AbstractDeployer::start(AbstractComponent * component, time_ns_t start_time) {
      if (get_architecture() == nullptr) return false;

      Task* task = nullptr;
      auto search = tasks.find(component);
      if (search != tasks.end()) {
        task = search->second;
        if (task == nullptr || task->get_status() != ACTIVATED) return false;
      }

      // create Barrier
      pthread_barrier_t barrier;
      pthread_barrierattr_t attr;
      pthread_barrierattr_init(&attr);
      int res = pthread_barrier_init(&barrier, &attr, 2);
      if (res != 0) {
        _logger->warn("pthread_barrier_init {} {}", errno, strerror(errno));
      }

      // start active tasks
      _logger->debug("start task {}", task->get_component()->name());
      trace_component_start(task->get_component());
      task->start(start_time, &barrier);

      pthread_barrier_wait(&barrier);

      // clear barrier
      pthread_barrier_destroy(&barrier);

      return true;
    }

    bool AbstractDeployer::start(std::string name, time_ns_t start_time) {
      for (auto tuple: tasks) {
        AbstractComponent* cpt = tuple.first;
        if (cpt->name() == name) {
          return start(cpt, start_time);
        }
      }
      return false;
    }

    bool AbstractDeployer::start_deployer() {
      if (get_architecture() == nullptr)
      return false;

      // start deployer
      struct timespec time;
      if (clock_gettime(MAUVE_CLOCK_ID, &time) == -1) {
        _logger->warn("clock_gettime {} {}", errno, strerror(errno));
      }
      start_time = timespec_to_ns(time);
      _logger->info("deployer started at {}", start_time);

      return true;
    }

    void AbstractDeployer::stop() {
      // stop tasks
      for (auto tuple: tasks) {
        Task* task = tuple.second;
        if (task != nullptr) {
          _logger->debug("stop task {}", task->get_component()->name());
          task->stop();
        }
      }
      start_time = 0;
    }

    void AbstractDeployer::stop(AbstractComponent * component) {
      auto search = tasks.find(component);
      if (search != tasks.end()) {
        Task* task = search->second;
        if (task != nullptr) {
          _logger->debug("stop task {}", task->get_component()->name());
          task->stop();
        }
      }
    }

    void AbstractDeployer::stop(std::string name) {
      for (auto tuple: tasks) {
        AbstractComponent* cpt = tuple.first;
        if (cpt->name() == name) {
          stop(cpt);
          break;
        }
      }
    }

    void AbstractDeployer::loop() {
      sigwaitinfo(&sig_set, NULL);
    }

    void AbstractDeployer::clear_tasks() {
      for (auto tuple: tasks) {
        Task* task = tuple.second;
        if (task != nullptr) {
          if (task->get_status() == RUNNING) {
            _logger->debug("stop task {}", task->get_component()->name());
            task->stop();
          }
          delete task;
        }
      }
      tasks.clear();
      start_time = 0;
    }

  }
}
