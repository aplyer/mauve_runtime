/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/HasPort.hpp"
#include "mauve/runtime/Port.hpp"
#include "mauve/runtime/EventPort.hpp"

#include <iostream>

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    HasPort::HasPort() {}

    // ------------------------- Destructor -------------------------

    HasPort::~HasPort() noexcept {
      for (AbstractPort* port: ports) {
        delete port;
      }
    }

    // ------------------------- Method -------------------------

    const std::vector<AbstractPort*> HasPort::get_ports() const {
      std::vector<AbstractPort*> vect;
      for (AbstractPort* port: ports) {
        vect.push_back(port);
      }
      return vect;
    }

    AbstractPort* HasPort::get_port(std::string const & name) const {
      for (AbstractPort* port: ports) {
        if (port->name == name) {
          return port;
        }
      }
      return nullptr;
    }

    void HasPort::disconnect() {
      for (AbstractPort* port: ports) {
        port->disconnect();
      }
    }

    EventPort & HasPort::mk_event_port(std::string const & name) {
      if (get_port(name) != nullptr) {
        throw new AlreadyDefinedPort(name);
      }
      EventPort * port = new EventPort{ this, name };
      ports.push_back(port);
      return *port;
    }

  }
}
