# MAUVE Runtime

This package provides the base layer of the MAUVE toolchain: the middleware
to implement components and resources and execute them in realtime

MAUVE Runtime is licensed under the
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Instal Instructions

General instructions are given in the [Toolchain Readme](https://gitlab.com/mauve/mauve_toolchain).

To install only the MAUVE Runtime package:
```
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/mauve_runtime.git src/mauve_runtime
catkin_make
```

## Documentation

The reference manual, the API documentation and some tutorials are available on https://mauve.gitlab.io
