/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WRITE_PORT_HP
#define MAUVE_RUNTIME_WRITE_PORT_HP

#include "Port.hpp"

namespace mauve {
  namespace runtime {

    template <typename T>
    class WriteService;

    template <typename T>
    class WritePort final : public Port<WriteService<T>> {
    public:
      friend class HasPort;
      WritePort() = delete;
      WritePort(WritePort const & other) = delete;

      void write(T value) const;
      inline void operator()(T value) const { write(value); };

    private:
      WritePort(HasPort* container, std::string const & name);
      ~WritePort() noexcept;
    };

  }
} /* namespace mauve */

#include "ipp/WritePort.ipp"

#endif
