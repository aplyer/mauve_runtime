/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WITH_HOOK_HPP
#define MAUVE_RUNTIME_WITH_HOOK_HPP

#include "Configurable.hpp"

namespace mauve {
  namespace runtime {

    /** Objects with hooks */
    struct WithHook {
    protected:
      /**
      * Hook function called when configuring the shell.
      * Redefine this function in Shell subclasses to specify the behavior of your shell when being configured.
      * \return true if configure succeeds
      */
      inline virtual bool configure_hook() { return true; };

      /**
      * Hook function called when cleaning the shell.
      * Redefine this function in Shell subclasses to specify the behavior of your shell when being cleaned up.
      */
      inline virtual void cleanup_hook() {};
    };
  }
}
#endif
