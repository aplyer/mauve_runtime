/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_TASK_HPP
#define MAUVE_RUNTIME_TASK_HPP

#include <mqueue.h>
#include <pthread.h>
#include <signal.h>
#include <iostream>
#include <string>
#include <atomic>

#include "common.hpp"

#define TASK_MQ_MAX_MSG 10
#define TASK_SIG SIGUSR1

namespace mauve {
  namespace runtime {

    class AbstractComponent;
    class ComponentLogger;

    // ========================= Task =========================

    enum TaskStatus { CREATED, ACTIVATED, RUNNING };

    /**
    * A system Task that executes a component.
    */
    class Task final {

    public:
      /**
      * Create a task associated to a component.
      * \param component the component owned by the task
      */
      Task(AbstractComponent * component);
      /** Task Destructor. */
      ~Task();

      /**
      * Get the component associated to this task.
      * \return a pointer to the component
      */
      AbstractComponent * get_component();

      inline TaskStatus get_status() const { return status; }

      /**
      * Get the thread ID of this task.
      * \return the thread ID
      */
      pthread_t thread_id() const;
      /**
      * Get the clock of the task.
      * \return currect time
      */
      time_ns_t get_time() const;

      /**
      * Activate the task.
      * \return true if activation succeeds.
      */
      bool activate();
      /**
      * Start the task.
      * \param start_time the desired start time of the task
      * \param barrier a starting barrier
      * \return true if starting succeeds.
      */
      bool start(time_ns_t start_time, pthread_barrier_t * barrier = nullptr);
      /**
      * Stop the task.
      * \return true if stopping succeeds
      */
      bool stop();

      /**
      * Get a string representation of this task.
      * \return a string
      */
      std::string to_string() const;

    protected:
      /** A pointer to a logger. */
      ComponentLogger* logger;

    private:
      static void *thread_start(void *data);

      void release();
      void run();

      AbstractComponent * component;
      std::atomic<TaskStatus> status;

      std::atomic<bool> finish;

      pthread_t pthread_id; // Useless
      pthread_t pt_id;      // Thread ID

      time_ns_t time;

      // barrier for configuration
      pthread_barrier_t barrier;
      // barrier for start
      std::atomic<pthread_barrier_t *> start_barrier;

      // Signal
      pid_t t_id;
      struct sigevent sev;
      timer_t timerid;
      struct itimerspec its;

    }; /* class Task */

  }
} /* namespace mauve */

#endif
