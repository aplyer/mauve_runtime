/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_EVENT_SERVICE_HPP
#define MAUVE_RUNTIME_EVENT_SERVICE_HPP

#include <functional>

#include "Service.hpp"

namespace mauve {
  namespace runtime {

    template <typename C>
    struct ServiceContainer;

    class EventService: public Service {
    public:
      EventService(std::string const & name): Service{name} {}
      virtual ~EventService() noexcept {}

      EventService() = delete;
      EventService(const EventService & other) = delete;

      virtual void react() const = 0;
      inline void operator()() const { react(); }
    };

    template <typename CORE>
    class EventServiceImpl final: public EventService {
    public:
      using action_t = std::function<void(CORE *)>;

      EventServiceImpl(ServiceContainer<CORE> * container, std::string const & name, action_t action);
      ~EventServiceImpl() noexcept;

      EventServiceImpl() = delete;
      EventServiceImpl(const EventServiceImpl<CORE> & other) = delete;

      void react() const override;

    private:
      ServiceContainer<CORE> * container;
      action_t action;
    };

  }
} // namespace

#include "ipp/EventService.ipp"

#endif
