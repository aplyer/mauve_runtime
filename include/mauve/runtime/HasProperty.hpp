/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_PROPERTY_HPP
#define MAUVE_RUNTIME_HAS_PROPERTY_HPP

#include "mauve/runtime/Configurable.hpp"
#include "mauve/runtime/Property.hpp"
#include <vector>

namespace mauve {
  namespace runtime {

    class HasProperty : virtual public Configurable {
    public:
      HasProperty();
      virtual ~HasProperty() noexcept;

      /**
       * Get the properties of the shell.
       * \return a vector of properties
       */
      const std::vector<AbstractProperty*> get_properties() const;

      AbstractProperty* get_property(std::string const & name) const;

    protected:
      /**
       * Create a new property.
       * \tparam T property type
       * \param name the property name
       * \param init_value the property initial value
       * \return a reference to a Property object
       */
      template <typename T>
      Property<T> & mk_property(std::string const & name, T init_value);

    private:
      std::vector<AbstractProperty *> properties;
    };

    // -------------------- Exceptions --------------------

    /** Exception for Already Defined Properties */
    struct AlreadyDefinedProperty: public std::exception {
      AlreadyDefinedProperty(std::string const & name) throw() : name(name) {}
      /** Exception name */
      const std::string name;
      /** Exception explanation */
      virtual const char* what() const throw() {
        std::string message = "Already Defined Property " + name;
        return message.c_str();
      }
    };

  }
}

#include "mauve/runtime/ipp/HasProperty.ipp"

#endif
