/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_COMPONENT_IPP
#define MAUVE_RUNTIME_COMPONENT_IPP

#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/Architecture.hpp"
#include "mauve/runtime/FiniteStateMachine.hpp"
#include "mauve/runtime/State.hpp"
#include "mauve/runtime/Task.hpp"

#include <unistd.h>

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    template <typename SHELL, typename CORE, typename FSM>
    Component<SHELL, CORE, FSM>::Component(std::string const & name)
    : AbstractComponent { name }
    , _logger           { new ComponentLogger(this) }
    , _shell            { nullptr }
    , _core             { nullptr }
    , _fsm              { nullptr }
    , current           { nullptr }
    , _task             { nullptr }
    , _priority         { 1 }
    , _cpu              { 0 }
    {
    }

    // ------------------------- Destructor -------------------------

    template <typename SHELL, typename CORE, typename FSM>
    Component<SHELL, CORE, FSM>::~Component() noexcept {
      delete _logger;
      cleanup();
      clear();
    }

    // ------------------------- Operator -------------------------

    // ------------------------- Method -------------------------

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::is_empty() const {
      return (_shell == nullptr) && (_core == nullptr) && (_fsm == nullptr);
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::is_configured() const {
      if ((_shell == nullptr) || (_core == nullptr) || (_fsm == nullptr)) return false;
      return _shell->is_configured() && _core->is_configured() && _fsm->is_configured();
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::is_activated() const {
      if (_task == nullptr) return false;
      return _task->get_status() == ACTIVATED;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::is_running() const {
      if (_task == nullptr) return false;
      return _task->get_status() == RUNNING;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::configure() {
      if (!configure_shell()) return false;
      if (!configure_core())  return false;
      if (!configure_fsm())   return false;
      DeployerLogger().info("Component {} configured", name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    void Component<SHELL, CORE, FSM>::cleanup() {
      cleanup_fsm();;
      cleanup_core();
      cleanup_shell();
      DeployerLogger().info("Component {} cleaned up", name());
    }

    template <typename SHELL, typename CORE, typename FSM>
    void Component<SHELL, CORE, FSM>::disconnect() {
      if (_shell == nullptr) return;
      _shell->disconnect();
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::clear() {
      if (!clear_fsm())   return false;
      if (!clear_core())  return false;
      if (!clear_shell()) return false;
      DeployerLogger().info("Component {} cleared", name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename ...P>
    bool Component<SHELL, CORE, FSM>::make(P... parameters) {
      return make<SHELL, CORE, FSM>(parameters...);
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename S, typename C, typename F, typename ...P>
    bool Component<SHELL, CORE, FSM>::make(P... parameters) {
      if (!is_empty())                   return false;
      if (!make_shell<S>(parameters...)) return false;
      if (!make_core<C>())               return false;
      if (!make_fsm<F>())                return false;
      DeployerLogger().info("Component {} made", name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    void Component<SHELL, CORE, FSM>::set_task(Task* task) {
      _task = task;
    }

    template <typename SHELL, typename CORE, typename FSM>
    time_ns_t Component<SHELL, CORE, FSM>::get_time() const {
      return _task->get_time();
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::set_priority(int priority) {
      if (is_activated() || is_running()) return false;
      if (priority < MIN_PRIO) return false;
      if (priority > MAX_PRIO) return false;
      _priority = priority;
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::set_cpu(int cpu) {
      if (is_activated() || is_running()) return false;
      if (cpu < 0) return false;
      if (cpu >= sysconf(_SC_NPROCESSORS_ONLN)) return false;
      _cpu = cpu;
      return true;
    }

    // ---------- Shell ----------

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::configure_shell() {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core == nullptr)        return false;
      if (_core->is_configured())  return false;
      DeployerLogger().debug("Configuring shell {} of component {}", _shell->type_name(), name());
      return _shell->_configure();
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::cleanup_shell() {
      if (_shell == nullptr)        return false;
      if (!_shell->is_configured()) return false;
      if (_core == nullptr)         return false;
      if (_core->is_configured())   return false;
      DeployerLogger().debug("Cleaning up shell {} of component {}", _shell->type_name(), name());
      _shell->_cleanup();
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::clear_shell() {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core != nullptr)        return false;
      DeployerLogger().debug("Clearing shell {} of component {}", _shell->type_name(), name());
      delete _shell;
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename ...P>
    bool Component<SHELL, CORE, FSM>::make_shell(P... parameters) {
      return make_shell<SHELL>(parameters...);
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename S, typename ...P>
    bool Component<SHELL, CORE, FSM>::make_shell(P... parameters) {
      if (_shell != nullptr) return false;
      if (_core != nullptr)  return false;
      _shell = new S(parameters...);
      _shell->_container = this;
      DeployerLogger().debug("Made shell {} of component {}", _shell->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename S, typename ...P>
    bool Component<SHELL, CORE, FSM>::replace_shell(P... parameters) {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core == nullptr)        return false;
      if (_core->is_configured())  return false;
      DeployerLogger().debug("Replacing shell {} of component {}", _shell->type_name(), name());
      delete _shell;
      _shell = new S(parameters...);
      _shell->_container = this;
      return true;
    }

    // ---------- Core ----------

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::configure_core() {
      if (_shell == nullptr)        return false;
      if (!_shell->is_configured()) return false;
      if (_core == nullptr)         return false;
      if (_core->is_configured())   return false;
      if (_fsm == nullptr)          return false;
      if (_fsm->is_configured())    return false;
      DeployerLogger().debug("Configuring core {} of component {}", _core->type_name(), name());
      return _core->_configure();
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::cleanup_core() {
      if (_shell == nullptr)        return false;
      if (!_shell->is_configured()) return false;
      if (_core == nullptr)         return false;
      if (!_core->is_configured())  return false;
      if (_fsm == nullptr)          return false;
      if (_fsm->is_configured())    return false;
      _core->_cleanup();
      DeployerLogger().debug("Cleaning up core {} of component {}", _core->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::clear_core() {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core == nullptr)        return false;
      if (_core->is_configured())  return false;
      if (_fsm != nullptr)         return false;
      DeployerLogger().debug("Clearing core {} of component {}", _core->type_name(), name());
      delete _core;
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename ...P>
    bool Component<SHELL, CORE, FSM>::make_core(P... parameters) {
      return make_core<CORE>(parameters...);
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename C, typename ...P>
    bool Component<SHELL, CORE, FSM>::make_core(P... parameters) {
      if (_shell == nullptr) return false;
      if (_core != nullptr)  return false;
      if (_fsm != nullptr)   return false;
      _core = new C(parameters...);
      _core->_container = this;
      DeployerLogger().debug("Made core {} of component {}", _core->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename C, typename ...P>
    bool Component<SHELL, CORE, FSM>::replace_core(P... parameters) {
      if (_shell == nullptr)      return false;
      if (_core == nullptr)       return false;
      if (_core->is_configured()) return false;
      if (_fsm == nullptr)        return false;
      if (_fsm->is_configured())  return false;
      DeployerLogger().debug("Replacing core {} of component {}", _core->type_name(), name());
      delete _core;
      _core = new C(parameters...);
      _core->_container = this;
      return true;
    }

    // ---------- Finite State Machine ----------

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::configure_fsm() {
      if (_core == nullptr)        return false;
      if (!_core->is_configured()) return false;
      if (_fsm == nullptr)         return false;
      if (_fsm->is_configured())   return false;
      DeployerLogger().debug("Configuring FSM {} of component {}", _fsm->type_name(), name());
      if (!_fsm->_configure())      return false;
      current = _fsm->initial;
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::cleanup_fsm() {
      if (_core == nullptr)        return false;
      if (!_core->is_configured()) return false;
      if (_fsm == nullptr)         return false;
      if (!_fsm->is_configured())  return false;
      DeployerLogger().debug("Cleaning up FSM {} of component {}", _fsm->type_name(), name());
      _fsm->_cleanup();
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::clear_fsm() {
      if (_core == nullptr)       return false;
      if (_core->is_configured()) return false;
      if (_fsm == nullptr)        return false;
      if (_fsm->is_configured())  return false;
      DeployerLogger().debug("Clearing FSM {} of component {}", _fsm->type_name(), name());
      delete _fsm;
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename ...P>
    bool Component<SHELL, CORE, FSM>::make_fsm(P... parameters) {
      return make_fsm<FSM>(parameters...);
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename F, typename ...P>
    bool Component<SHELL, CORE, FSM>::make_fsm(P... parameters) {
      if (_core == nullptr) return false;
      if (_fsm != nullptr)  return false;
      _fsm = new F(parameters...);
      _fsm->_container = this;
      DeployerLogger().debug("Made FSM {} of component {}", _fsm->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    template <typename F, typename ...P>
    bool Component<SHELL, CORE, FSM>::replace_fsm(P... parameters) {
      if (_core == nullptr)      return false;
      if (_fsm == nullptr)       return false;
      if (_fsm->is_configured()) return false;
      delete _fsm->_logger;
      DeployerLogger().debug("Replacing FSM {} of component {}", _fsm->type_name(), name());
      delete _fsm;
      _fsm = new F(parameters...);
      _fsm->_container = this;
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::step() {
      if (_task != nullptr) if (_task->get_status() == ACTIVATED || _task->get_status() == RUNNING) return false;
      if (!is_configured())   return false;
      if (current == nullptr) return false;
      current = current->run(this->_core);
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    bool Component<SHELL, CORE, FSM>::run() {
      if (_task == nullptr)               return false;
      if (_task->get_status() != RUNNING) return false;
      if (current == nullptr)             return false;
      current = current->run(this->_core);
      return true;
    }

    template <typename SHELL, typename CORE, typename FSM>
    AbstractState * Component<SHELL, CORE, FSM>::current_state() {
      return current;
    }

  }
}

#endif
