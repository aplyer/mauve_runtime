/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_DEPLOYER_IPP
#define MAUVE_RUNTIME_DEPLOYER_IPP

#include "mauve/runtime/Deployer.hpp"
#include "mauve/runtime/Architecture.hpp"
#include "mauve/runtime/Manager.hpp"

namespace mauve {
  namespace runtime {

    template <typename ARCHI>
    AbstractDeployer* mk_abstract_deployer(ARCHI* a, Manager<ARCHI>* m) {
      AbstractDeployer::deployer = new Deployer<ARCHI>(a, m);
      return AbstractDeployer::instance();
    }

    template <typename ARCHI>
    Deployer<ARCHI> * mk_deployer(ARCHI* a, Manager<ARCHI>* m) {
      auto d = mk_abstract_deployer<ARCHI>(a, m);
      return dynamic_cast< Deployer<ARCHI>* >(d);
    }

    // ------------------------- Constructor -------------------------
    template <typename ARCHI>
    Deployer<ARCHI>::Deployer(ARCHI * architecture, Manager<ARCHI> * manager)
    : AbstractDeployer {}
    , _architecture    { architecture }
    , manager          { manager }
    {
    }

    // ------------------------- Destructor -------------------------

    template <typename ARCHI>
    Deployer<ARCHI>::~Deployer() noexcept {}

    template <typename ARCHI>
    Architecture* Deployer<ARCHI>::get_architecture() {
      return _architecture;
    }

    // ---------- Manager ----------

    template <typename ARCHI>
    std::vector<std::string> Deployer<ARCHI>::manager_actions() const {
      if (manager == nullptr)
      return std::vector<std::string>();
      return manager->actions_name();
    }

    template <typename ARCHI>
    bool Deployer<ARCHI>::manager_apply(std::string name) {
      if (manager == nullptr)
      return false;
      return manager->apply(this, name);
    }

  }
}

#endif
