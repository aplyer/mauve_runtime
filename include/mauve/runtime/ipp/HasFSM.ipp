/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_FSM_IPP
#define MAUVE_RUNTIME_HAS_FSM_IPP

#include "mauve/runtime/HasFSM.hpp"
#include "mauve/runtime/FiniteStateMachine.hpp"

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    template <typename FSM>
    HasFSM<FSM>::HasFSM()
    : _fsm { nullptr } {}

    // ------------------------- Destructor -------------------------

    template <typename FSM>
    HasFSM<FSM>::~HasFSM() noexcept {
      delete _fsm;
    }

    // ------------------------- Methods -------------------------

    template <typename FSM>
    AbstractFiniteStateMachine* HasFSM<FSM>::get_fsm() const {
      return _fsm;
    }

    template <typename FSM>
    FSM& HasFSM<FSM>::fsm() const {
      return *_fsm;
    }

    template <typename FSM>
    bool HasFSM<FSM>::clear_fsm() {
      if (_fsm != nullptr && _fsm->is_configured()) {
        return false;
      }
      delete _fsm;
      return true;
    }

    template <typename FSM>
    template <typename S>
    bool HasFSM<FSM>::create_fsm() {
      if (_fsm != nullptr) {
        return false;
      }
      _fsm = new S();
      return true;
    }

  }
}

#endif
