/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SHARED_DATA_IPP
#define MAUVE_RUNTIME_SHARED_DATA_IPP

#include "mauve/runtime/SharedData.hpp"
#include "mauve/runtime/ReadPort.hpp"

#include <typeinfo>
#include <cxxabi.h>
#include <sstream>

namespace mauve {
  namespace runtime {

    // ========================= Shell =========================

    template <typename T>
    SharedDataShell<T>::SharedDataShell(T value)
    : Shell()
    , default_value { mk_property("default_value", value) }
    {
    }

    // ========================= Core =========================

    template <typename T>
    SharedDataCore<T>::SharedDataCore()
    : Core<SharedDataShell<T>> {}
    , status                   { DataStatus::NO_DATA }
    , mutex                    {}
    {
    }

    template <typename T>
    bool SharedDataCore<T>::configure_hook() {
      value = this->shell().default_value.get_value();
      return true;
    }
    // ---------- Reader ----------

    template <typename T>
    StatusValue<T> SharedDataCore<T>::read() {
      mutex.lock();
      StatusValue<T> res { status, value };
      if (status == DataStatus::NEW_DATA)
        status = DataStatus::OLD_DATA;
      mutex.unlock();
      return res;
    }

    template <typename T>
    T SharedDataCore<T>::read_value() {
      mutex.lock();
      T res = value;
      if (status == DataStatus::NEW_DATA)
        status = DataStatus::OLD_DATA;
      mutex.unlock();
      return res;
    }

    template <typename T>
    DataStatus SharedDataCore<T>::read_status() {
      mutex.lock();
      DataStatus res = status;
      mutex.unlock();
      return res;
    }

    // ---------- Writer ----------

    template <typename T>
    void SharedDataCore<T>::write(T value) {
      mutex.lock();
      this->value = value;
      status = DataStatus::NEW_DATA;
      mutex.unlock();
    }

    // ---------- Event ----------

    template <typename T>
    void SharedDataCore<T>::reset() {
      mutex.lock();
      value = this->shell().default_value.get_value();
      status = DataStatus::NO_DATA;
      mutex.unlock();
    }

    // ========================= Interface =========================

    // ========================= Resource =========================

    // template <typename T>
    // std::string SharedData<T>::data_type() const {
    //   int status = -4;
    //   return abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
    // }
    //
    // template <typename T>
    // std::string SharedData<T>::display() const {
    //   if (status == DataStatus::NO_DATA)
    //   return "[]";
    //   std::ostringstream os;
    //   os << "[" << value << "]";
    //   return os.str();
    // }


  }
} /* namespace mauve */

#endif
