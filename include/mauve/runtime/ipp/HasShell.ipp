/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_SHELL_IPP
#define MAUVE_RUNTIME_HAS_SHELL_IPP

#include "mauve/runtime/HasShell.hpp"
#include "mauve/runtime/Shell.hpp"

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    template <typename SHELL>
    HasShell<SHELL>::HasShell()
    : _shell { nullptr } {}

    // ------------------------- Destructor -------------------------

    template <typename SHELL>
    HasShell<SHELL>::~HasShell() noexcept {
      if (_shell != nullptr) {
        delete _shell;
      }
    }

    template <typename SHELL>
    template <typename S>
    bool HasShell<SHELL>::create_shell() {
      if (_shell != nullptr) {
        return false;
      }
      _shell = new S();
      return true;
    }
  }
}

#endif
