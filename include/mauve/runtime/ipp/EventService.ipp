/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_EVENT_SERVICE_IPP
#define MAUVE_RUNTIME_EVENT_SERVICE_IPP

#include "mauve/runtime/EventService.hpp"
#include "mauve/runtime/ServiceContainer.hpp"
#include "mauve/runtime/tracing.hpp"

namespace mauve {
  namespace runtime {

    // -------------------- Constructor --------------------

    template <typename CORE>
    EventServiceImpl<CORE>::EventServiceImpl(ServiceContainer<CORE> * container, std::string const & name, action_t action)
    : EventService { name }
    , container { container }
    , action  { action }
    {
    }

    // -------------------- Destructor --------------------

    template <typename CORE>
    EventServiceImpl<CORE>::~EventServiceImpl() noexcept
    {
    }

    // -------------------- Method --------------------

    template <typename CORE>
    void EventServiceImpl<CORE>::react() const {
      container->logger().trace("calling EventService {}", this->name);
      trace_service_begin(container->core_ptr()->container_name(), this->name);
      action(container->core_ptr());
      trace_service_end(container->core_ptr()->container_name(), this->name);
    }

  }
}

#endif
