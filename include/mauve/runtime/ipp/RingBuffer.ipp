/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_RING_BUFFER_IPP
#define MAUVE_RUNTIME_RING_BUFFER_IPP

#include "mauve/runtime/RingBuffer.hpp"
#include "mauve/runtime/ReadPort.hpp"
#include "mauve/runtime/WritePort.hpp"
#include <typeinfo>
#include <cxxabi.h>
#include <sstream>

namespace mauve {
  namespace runtime {

  // -------------------- Constructor --------------------

  template <typename T>
  RingBuffer<T>::RingBuffer(std::string const & name, size_t size, T default_value)
  : Resource      { name }
  , default_value { default_value }
  , size          { size }
  , begin         { 0 }
  , end           { 0 }
  , status        { DataStatus::NO_DATA }
  {
    values = new T[size];
    for (int i = 0; i < size; i++) {
      values[i] = default_value;
    }
  }

  // -------------------- Destructor --------------------

  template <typename T>
  RingBuffer<T>::~RingBuffer() noexcept {
    delete[] values;
  }

  // -------------------- Method --------------------

  template <typename T>
  std::string RingBuffer<T>::type_name() const {
    int status = -4;
    return abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
  }

  template <typename T>
  std::string RingBuffer<T>::display() const {
    if (status == DataStatus::NO_DATA) {
      return "[]";
    }
    std::ostringstream os;
    os << "[";
    size_t index = begin;
    os <<  values[index];
    while (index != end) {
      index = (index+1) % size;
      os << ", " << values[index];
    }
    return os.str();
  }


  template <typename T>
  void RingBuffer<T>::clear() {
    status = DataStatus::NO_DATA;
    begin = end = 0;
    for (int i = 0; i < size; i++) {
      values[i] = default_value;
    }
  }

  // ---------- Reader ----------

  template <typename T>
  DataStatus RingBuffer<T>::read_status_action() {
    lock();
    DataStatus res = status;
    unlock();
    return res;
  }

  template <typename T>
  StatusValue<T> RingBuffer<T>::read_action() {
    lock();
    DataStatus status = this->status;
    T value;
    if (this->status == DataStatus::NO_DATA) {
      value = default_value;
    }
    else if (begin == end) {
      this->status = DataStatus::OLD_DATA;
      value = values[begin];
    }
    else {
      size_t const prev = begin;
      begin = (begin + 1) % size;
      value = values[prev];
    }
    unlock();
    return StatusValue<T>{ status, value };
  }

  template <typename T>
  T RingBuffer<T>::read_value_action() {
    lock();
    T value;
    if (this->status == DataStatus::NO_DATA) {
      value = default_value;
    }
    else if (begin == end) {
      this->status = DataStatus::OLD_DATA;
      value = values[begin];
    }
    else {
      size_t const prev = begin;
      begin = (begin + 1) % size;
      value = values[prev];
    }
    unlock();
    return value;
  }

  // ---------- Writer ----------

  template <typename T>
  void RingBuffer<T>::write_action(T value) {
    lock();
    if (status != DataStatus::NEW_DATA) {
      values[end] = value;
      status = DataStatus::NEW_DATA;
    }
    else {
      end = (end + 1) % size;
      if (end == begin) {
        begin = (begin + 1) % size;
      }
      values[end] = value;
      status = DataStatus::NEW_DATA;
    }
    unlock();
  }

  // ---------- Reset ----------

  template <typename T>
  void RingBuffer<T>::reset_action() {
    lock();
    status = DataStatus::NO_DATA;
    begin = end = 0;
    for (int i = 0; i < size; i++) {
      values[i] = default_value;
    }
    unlock();
  }

}} /* namespace mauve */

#endif
