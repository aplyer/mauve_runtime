/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_FINITE_STATE_MACHINE_IPP
#define MAUVE_RUNTIME_FINITE_STATE_MACHINE_IPP

#include "mauve/runtime/FiniteStateMachine.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/ExecState.hpp"
#include "mauve/runtime/SynchroState.hpp"

#include <typeinfo>
#include <cxxabi.h>
#include <set>


namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    template <typename SHELL, typename CORE>
    FiniteStateMachine<SHELL, CORE>::FiniteStateMachine()
    : AbstractFiniteStateMachine {}
    , _container                 { nullptr }
    , _configured                { false }
    , initial                    { nullptr }
    {
    }

    // ------------------------- Destructor -------------------------

    template <typename SHELL, typename CORE>
    FiniteStateMachine<SHELL, CORE>::~FiniteStateMachine() noexcept {
      _cleanup();
      for (State<CORE> * state: states) {
        delete state;
      }
    }

    // ------------------------- Method -------------------------

    template <typename SHELL, typename CORE>
    std::string FiniteStateMachine<SHELL, CORE>::shell_type_name() const {
      int status = -4; // some arbitrary value to eliminate the compiler warning
      std::string shell_name = abi::__cxa_demangle(typeid(SHELL).name(), nullptr, nullptr, &status);
      return shell_name;
    }

    template <typename SHELL, typename CORE>
    std::string FiniteStateMachine<SHELL, CORE>::core_type_name() const {
      int status = -4; // some arbitrary value to eliminate the compiler warning
      std::string core_name = abi::__cxa_demangle(typeid(CORE).name(), nullptr, nullptr, &status);
      return core_name;
    }

    template <typename SHELL, typename CORE>
    AbstractState* FiniteStateMachine<SHELL, CORE>::get_state(std::string const & name) const {
      for (State<CORE>* state: states) {
        if (state->name == name) {
          return state;
        }
      }
      return nullptr;
    }

    template <typename SHELL, typename CORE>
    ExecState<CORE> & FiniteStateMachine<SHELL, CORE>::mk_execution(std::string const & name, Update_t fun) {
      ExecState<CORE> * state = new ExecState<CORE>(this, name, fun);
      states.push_back(state);
      if (initial == nullptr) {
        initial = state;
      }
      return *state;
    }

    template <typename SHELL, typename CORE>
    SynchroState<CORE> & FiniteStateMachine<SHELL, CORE>::mk_synchronization(std::string const & name, time_ns_t clock) {
      SynchroState<CORE> * state = new SynchroState<CORE>(this, name, clock);
      states.push_back(state);
      if (initial == nullptr) {
        initial = state;
      }
      return *state;
    }

    template <typename SHELL, typename CORE>
    void FiniteStateMachine<SHELL, CORE>::set_initial(State<CORE> & state) {
      initial = &state;
    }

    template <typename SHELL, typename CORE>
    void FiniteStateMachine<SHELL, CORE>::set_next(State<CORE> & state, State<CORE> & next) {
      state.set_next(next);
    }

    template <typename SHELL, typename CORE>
    void FiniteStateMachine<SHELL, CORE>::mk_transition(ExecState<CORE> & state, Guard_t guard, State<CORE> & next) {
      state.mk_transition(guard, next);
    }

    template <typename SHELL, typename CORE>
    bool FiniteStateMachine<SHELL, CORE>::check_reachable() const {
      std::set<State<CORE>*> reached;
      std::vector<State<CORE>*> todo;

      todo.push_back(initial);

      while (!todo.empty()) {
        State<CORE>* state = todo.back();
        todo.pop_back();
        reached.insert(state);

        for (State<CORE>* n: state->next_states()) {
          if (reached.find(n) == reached.end()) {
            todo.push_back(n);
          }
        }
      }

      for (State<CORE>* s: states) {
        if (reached.find(s) == reached.end()) {
          return false;
        }
      }

      return true;
    }

    template <typename SHELL, typename CORE>
    bool FiniteStateMachine<SHELL, CORE>::_configure() {
      if (is_configured()) return true;
      _configured = configure_hook();
      return _configured;
    }

    template <typename SHELL, typename CORE>
    void FiniteStateMachine<SHELL, CORE>::_cleanup() {
      if (is_configured()) {
        cleanup_hook();
        _configured = false;
      }
    }

  }
} /* namespace mauve */

#endif
