/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_OTHER_PROPERTY_IPP
#define MAUVE_RUNTIME_OTHER_PROPERTY_IPP

#include "mauve/runtime/Property.hpp"

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  template <typename T>
  OtherProperty<T>::OtherProperty(HasProperty* container, std::string const & name, T init_value)
  : AbstractProperty { container, name }
  , value            {init_value}
  {}

  // ------------------------- Destructor -------------------------

  template <typename T>
  OtherProperty<T>::~OtherProperty() noexcept {}

  // ------------------------- Method -------------------------

  template <typename T>
  std::string OtherProperty<T>::type_name() const {
    int status = -4;
    std::string type_name = abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
    return type_name;
  }

  template <typename T>
  property_type OtherProperty<T>::get_type() const {
    return OTHER;
  }

  template <typename T>
  T OtherProperty<T>::get_value() const {
    return value;
  }

  template <typename T>
  bool OtherProperty<T>::set_value(T value) {
    if (this->container->is_configured()) {
      return false;
    }
    this->value = value;
    return true;
  }

  // ------------------------- Operator -------------------------

  template <typename T>
  OtherProperty<T>::operator T&() {
    return value;
  }

  template <typename T>
  OtherProperty<T>::operator T() const {
    return value;
  }

  template <typename T>
  OtherProperty<T> & OtherProperty<T>::operator=(T value) {
    set_value(value);
    return *this;
  }

  template <typename T>
  OtherProperty<T> & OtherProperty<T>::operator=(OtherProperty<T> const & other) {
    set_value(other.value);
    return *this;
  }

}}

#endif
