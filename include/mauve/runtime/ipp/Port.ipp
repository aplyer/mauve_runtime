/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_PORT_IPP
#define MAUVE_RUNTIME_PORT_IPP

#include "mauve/runtime/Port.hpp"
#include "mauve/runtime/HasPort.hpp"
#include <algorithm>
#include <iostream>

namespace mauve {
  namespace runtime {

    // -------------------- Constructor --------------------

    template <typename S>
    Port<S>::Port(HasPort* container, std::string const & name)
    : AbstractPort { name }
    , container    { container }
    {
    }

    // -------------------- Destructor --------------------

    template <typename S>
    Port<S>::~Port() noexcept {}

    // -------------------- Methods --------------------

    template <typename S>
    bool Port<S>::is_connected() const {
      return !(services.empty());
    }

    template <typename S>
    bool Port<S>::is_connected_to(S& service) const {
      for (S* serv: services) {
        if (serv == &service) {
          return true;
        }
      }
      return false;
    }

    template <typename S>
    bool Port<S>::connect(S& service) {
      if (container->is_configured()) return false;
      if (is_connected_to(service))   return false;
      services.push_back(&service);
      return true;
    }

    template <typename S>
    bool Port<S>::disconnect() {
      if (container->is_configured()) return false;
      services.clear();
      return true;
    }

    template <typename S>
    bool Port<S>::disconnect(S& service) {
      if (container->is_configured()) return false;
      typename std::vector<S*>::iterator position = std::find(services.begin(), services.end(), &service);
      if (position != services.end()) {
        services.erase(position);
        return true;
      }
      return false;
    }

    template <typename S>
    bool Port<S>::connect_service(Service* service) {
      if (S* serv = dynamic_cast<S*>(service)) {
        return connect(*serv);
      }
      return false;
    }

    template <typename S>
    std::vector<Service*> Port<S>::connected_services() const {
      std::vector<Service*> res;
      for (S* service: services) {
        res.push_back(service);
      }
      return res;
    }
  }
}

#endif
