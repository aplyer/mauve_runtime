/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_EXEC_STATE_HPP
#define MAUVE_RUNTIME_EXEC_STATE_HPP

#include "State.hpp"

#include <string>
#include <vector>
#include <functional>

namespace mauve {
  namespace runtime {

  template <typename C>
  class Transition;

  template <typename CORE>
  class ExecState final : public State<CORE> {

  public:
    using Guard_t = std::function<bool(CORE *)>;
    using Update_t = std::function<void(CORE *)>;

    template <typename S, typename C>
    friend class FiniteStateMachine;

    ExecState() = delete;
    ExecState(ExecState<CORE> const & other) = delete;
    ExecState<CORE> & operator=(ExecState<CORE> const & other) = delete;

    bool is_execution() const override;
    bool is_synchronization() const override;

    time_ns_t get_clock() const override;
    bool set_clock(time_ns_t clock) override;

    bool set_update(Update_t update);

    std::string to_string() const override;

  private:
    ExecState(AbstractFiniteStateMachine* container, std::string const & name, Update_t update);
    ~ExecState() noexcept;

    void mk_transition(Guard_t guard, State<CORE> & next);
    std::vector<State<CORE>*> next_states() const override;

    Update_t update;
    std::vector<Transition<CORE> *> transitions;
    State<CORE> * run(CORE * core) override;
  };

}} // namespace

#include "ipp/ExecState.ipp"

#endif
