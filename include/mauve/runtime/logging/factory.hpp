/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SINK_HPP
#define MAUVE_RUNTIME_SINK_HPP

#include <spdlog/spdlog.h>

namespace YAML {
  class Node;
}

namespace mauve {
  namespace runtime {

  /**
   * Create a logger from its type.
   * \param label label of the logger
   * \param type type of the logger
   * \param args logger arguments
   * \return a pointer to the logger
   */
  std::shared_ptr<spdlog::logger> mk_logger(const std::string& label,
    const std::string& type, const YAML::Node& args);

  /**
   * Default log file name.
   * \return the default MAUVE log file name in /tmp
   */
  std::string default_file_name();

}}

#endif
