/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SERVICE_HPP
#define MAUVE_RUNTIME_SERVICE_HPP

#include <string>

namespace mauve {
  namespace runtime {

  /** A Service */
  class Service {
  public:
    Service() = delete;
    /** Constructor */
    Service(std::string const & name);
    /** Constructor */
    Service(const Service & other) = delete;
    virtual ~Service() noexcept;

    /** Service name */
    const std::string name;
    /** Service type name */
    std::string type_name() const;
  };

}} /* namespace mauve */

#endif
