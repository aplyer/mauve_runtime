/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WITH_ABSTRACT_INTERFACE_HPP
#define MAUVE_RUNTIME_WITH_ABSTRACT_INTERFACE_HPP

namespace mauve {
  namespace runtime {

    class AbstractInterface;

    /** Trait for objects with an abstract Interface */
    struct WithAbstractInterface {
      /** Get a pointer to the Interface */
      virtual AbstractInterface* get_interface () const = 0;
      /** Check if the Interface is empty */
      virtual bool is_empty_interface          () const = 0;
      /** Configure the Interface */
      virtual bool configure_interface         ()       = 0;
      /** Clean up the Interface */
      virtual bool cleanup_interface           ()       = 0;
    };
  }
}

#endif
