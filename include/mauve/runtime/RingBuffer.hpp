/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_RING_BUFFER_HPP
#define MAUVE_RUNTIME_RING_BUFFER_HPP

#include "DataStatus.hpp"
#include "Resource.hpp"
#include "ReadService.hpp"
#include "WriteService.hpp"

#include <tuple>

namespace mauve {
  namespace runtime {

  template <typename T>
  class ReadPort;

  template <typename T>
  class WritePort;

  template <typename T>
  class RingBuffer final: public Resource {
  public:
    RingBuffer() = delete;
    RingBuffer(std::string const & name, size_t size, T default_value);
    RingBuffer(RingBuffer const & other) = delete;
    ~RingBuffer() noexcept;

    const size_t size;
    const T default_value;

    void clear() override;

    std::string type_name() const;
    std::string display()   const override;

    // ---------- Reader ----------

    ReadService<StatusValue<T>> & read     = mk_read_service<StatusValue<T>>("read", [this](){ return read_action(); });
    ReadService<T> & read_value           = mk_read_service<T>("read_value", [this](){ return read_value_action(); });
    ReadService<DataStatus> & read_status = mk_read_service<DataStatus>("read_status", [this](){ return read_status_action(); });

    // ---------- Writer ----------

    WriteService<T> & write_service = mk_write_service<T>("write_service", [this](T value){ write_action(value); });

    // ---------- Event ----------

    EventService & reset = mk_event_service("reset", [this](){ reset_action(); });

  private:
    T * values;
    size_t begin;
    size_t end;
    DataStatus status;

    StatusValue<T> read_action();
    T read_value_action();
    DataStatus read_status_action();
    void write_action(T value);
    void reset_action();
  };

}} /* namespace mauve */

#include "ipp/RingBuffer.ipp"

#endif
