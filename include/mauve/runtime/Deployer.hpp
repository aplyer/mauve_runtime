/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_DEPLOYER_HPP
#define MAUVE_RUNTIME_DEPLOYER_HPP

#include <signal.h>
#include <vector>
#include <string>

#include "AbstractDeployer.hpp"

namespace mauve {
  namespace runtime {

    /**
    * Deployer class deploying a specific architecture.
    * \tparam ARCHI Architecture type
    */
    template <typename ARCHI>
    class Deployer final : public AbstractDeployer {
    public:

      // ---------- Architecture ----------

      Architecture* get_architecture() override;

      /**
      * Get the deployer architecture.
      * \return a pointer on the deployed architecture
      */
      inline ARCHI & architecture() const { return *_architecture; };

      // ---------- Manager ----------

      std::vector<std::string> manager_actions() const override;
      bool manager_apply(std::string name) override;

    private:
      template <typename A>
      friend AbstractDeployer* mk_abstract_deployer(A* a, Manager<A>* m);

      template <typename A>
      friend Deployer<A>* mk_deployer(A* a, Manager<A>* m);

      Deployer(ARCHI * architecture, Manager<ARCHI> * manager = nullptr);
      ~Deployer() noexcept;

      ARCHI * _architecture;
      Manager<ARCHI> * manager;
    };

    template <typename ARCHI>
    Deployer<ARCHI>* mk_deployer(ARCHI * architecture, Manager<ARCHI> * manager = nullptr);

  }
} // namespace

#include "ipp/Deployer.ipp"

#endif
