/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_CALL_PORT_HPP
#define MAUVE_RUNTIME_CALL_PORT_HPP

#include "Port.hpp"

namespace mauve {
  namespace runtime {

    template <typename R, typename ...P>
    class CallService;

    /** A Port to call a service */
    template <typename R, typename ...P>
    class CallPort final: public Port<CallService<R, P...>> {
    public:
      friend class HasPort;
      CallPort() = delete;
      CallPort(CallPort const & other) = delete;

      /** Default returned value */
      const R default_value;

      /** Call a service with a set of parameters */
      R call(P... parameters) const;

      /** Helper to call the service implicitely */
      inline R operator()(P... parameters) const { return call(parameters ...); };

    private:
      CallPort(HasPort* container, std::string const & name, R default_value);
      ~CallPort() noexcept;
    };

  }
} // namespace

#include "ipp/CallPort.ipp"

#endif
