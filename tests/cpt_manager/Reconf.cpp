/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "Reconf.hpp"
#include "Archi.hpp"

void ManagerCore::update() {
  StatusValue<bool> status = shell().status.read();
  if (status.status == DataStatus::NEW_DATA) {
    std::cout << "===> Manager " << container_name() << ": " << status.value << " <===\n";
    if (status.value == false) {
      auto& N = deployer().architecture().normal;
      auto& B = deployer().architecture().backup;
      auto& D = deployer().architecture().data;
      // Stop
      deployer().stop(N.name());
      deployer().stop(B.name());
      time_ns_t t_n = N.get_time();
      time_ns_t t_b = B.get_time();
      // Cleanup
      N.cleanup();
      B.cleanup();
      // Disconnect
      N.disconnect();
      B.disconnect();
      // bool res = B.replace_shell<ReaderShell>();
      // Connect backup
      B.shell().data.connect(D.interface().read_value);
      // Configure
      N.configure();
      B.configure();
      // Activate
      deployer().activate(N.name());
      deployer().activate(B.name());
      // Start
      deployer().start(N.name(), t_n);
      deployer().start(B.name(), t_b);
    }
    else {
      auto& N = deployer().architecture().normal;
      auto& B = deployer().architecture().backup;
      auto& D = deployer().architecture().data;
      // Stop
      deployer().stop(N.name());
      deployer().stop(B.name());
      time_ns_t t_n = N.get_time();
      time_ns_t t_b = B.get_time();
      // Cleanup
      N.cleanup();
      B.cleanup();
      // Disconnect
      N.disconnect();
      B.disconnect();
      // Connect backup
      N.shell().data.connect(D.interface().read_value);
      // Configure
      N.configure();
      B.configure();
      // Activate
      deployer().activate(N.name());
      deployer().activate(B.name());
      // Start
      deployer().start(N.name(), t_n);
      deployer().start(B.name(), t_b);    }
  }
}
