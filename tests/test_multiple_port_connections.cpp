/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/EventPort.hpp"
#include "mauve/runtime/ReadPort.hpp"
#include "mauve/runtime/WritePort.hpp"
#include "mauve/runtime/CallPort.hpp"

#include "mauve/runtime/EventService.hpp"
#include "mauve/runtime/ReadService.hpp"
#include "mauve/runtime/WriteService.hpp"
#include "mauve/runtime/CallService.hpp"

#include <iostream>

using namespace mauve::runtime;

int main(int argc, char const *argv[]) {

  // ----- Event Connection -----
  EventPort eventPort { "eventPort" };
  EventService eventService_1 { "eventService_1", []{ std::cout << "react_1" << std::endl; } };
  EventService eventService_2 { "eventService_2", []{ std::cout << "react_2" << std::endl; } };
  eventPort.connect(eventService_1);
  eventPort.connect(eventService_2);
  eventPort.react();

  // ----- Read Connection -----
  ReadPort<int> readPort { "readPort" , 0 };
  ReadService<int> readService_1 { "readService_1", []{ std::cout << "read_1" << std::endl; return 1; } };
  ReadService<int> readService_2 { "readService_2", []{ std::cout << "read_2" << std::endl; return 2; } };
  readPort.connect(readService_1);
  readPort.connect(readService_2);
  int read_res = readPort.read();
  std::cout << "result = " << read_res << std::endl;

  // ----- Write Connection -----
  WritePort<int> writePort { "writePort" };
  WriteService<int> writeService_1 { "writeService_1", [](int i){ std::cout << "write_1: " << i << std::endl; } };
  WriteService<int> writeService_2 { "writeService_2", [](int i){ std::cout << "write_2: " << i << std::endl; } };
  writePort.connect(writeService_1);
  writePort.connect(writeService_2);
  writePort.write(10);

  // ----- Call Connection -----
  CallPort<float, int> callPort { "callPort", 0.0f };
  CallService<float, int> callService_1 { "callService_1", [](int i){ std::cout << "write_1: " << i << std::endl; return -1.0 * i; } };
  CallService<float, int> callService_2 { "callService_2", [](int i){ std::cout << "write_2: " << i << std::endl; return -1.0 * i; } };
  callPort.connect(callService_1);
  callPort.connect(callService_2);
  float call_res = callPort.call(10);
  std::cout << "result = " << call_res << std::endl;

  return 0;
}
