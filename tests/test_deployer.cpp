/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "test_deployer.hpp"

using namespace mauve::runtime;

int main() {
  auto archi = new Archi();
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  level: info" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "data:" << std::endl;
  config << "  - type: stdout" << std::endl;
  config << "    level: trace" << std::endl;
  AbstractLogger::initialize(config);
  mk_abstract_deployer(archi);
  auto depl = AbstractDeployer::instance();
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();
  depl->loop();
  depl->stop();
}
