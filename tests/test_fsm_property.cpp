/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime.hpp"
#include <iostream>

using namespace mauve::runtime;

struct S: public Shell {};
struct C: public Core<S> {
  void update() {
    std::cout << "update" << std::endl;
  }
};

struct F : FiniteStateMachine<S, C> {
  Property<time_ns_t>& period = mk_property<time_ns_t>("period", sec_to_ns(1));
  ExecState<C>& exec_state = mk_execution("E", &C::update);
  SynchroState<C>& sync_state = mk_synchronization("S", period);

  bool configure_hook() override {
    this->set_initial(exec_state);
    this->set_next(exec_state, sync_state);
    this->set_next(sync_state, exec_state);
    return true;
  };
};

struct A: Architecture {
  Component<S, C, F> & cpt = mk_component<S, C, F>("cpt");

  bool configure_hook() override {
    cpt.fsm().period = sec_to_ns(2);
    cpt.configure();
    return true;
  }
};


int main(int argc, char const *argv[]) {
  A a;

  a.configure();
  std::cout << "period = " << a.cpt.fsm().period << std::endl;
  a.cpt.fsm().period = 1;
  std::cout << "period = " << a.cpt.fsm().period << std::endl;

  return 0;
}
